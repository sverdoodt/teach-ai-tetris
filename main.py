# TODO: Improve scoring
# This will increase performance as well
# TODO: Add stop and pause button

import pygame
import random

"""
First, global variables are defined, then the data structure for the pieces is defined and then functions.
"""

"""
The game will be played on a 10 x 20 square grid.
The shapes in Tetris are: S, Z, I, O, J, L & T,
hereafter represented in order by 0 - 6.
"""

pygame.font.init()

# GLOBALS VARS
s_width = 800
s_height = 700
play_width = 300  # meaning 300 // 10 = 30 width per block
play_height = 600  # meaning 600 // 20 = 30 height per block
block_size = 30

top_left_x = (s_width - play_width) // 2
top_left_y = s_height - play_height

# SHAPE FORMATS
""" These are nested lists, because shapes can have multiple rotations. """
S = [
    [".....",
     "......",
     "..00..",
     ".00...",
     "....."],
    [".....",
     "..0..",
     "..00.",
     "...0.",
     "....."],
]

Z = [
    [".....",
     ".....",
     ".00..",
     "..00.",
     "....."],
    [".....",
     "..0..",
     ".00..",
     ".0...",
     "....."],
]

I = [
    [".....",
     "0000.",
     ".....",
     ".....",
     "....."],
    ["..0..",
     "..0..",
     "..0..",
     "..0..",
     "....."],
]

O = [[".....",
      ".....",
      ".00..",
      ".00..",
      "....."]]

J = [
    [".....",
     "..0..",
     "..0..",
     ".00..",
     "....."],
    [".....",
     ".....",
     ".000.",
     "...0.",
     "....."],
    [".....",
     "..00.",
     "..0..",
     "..0..",
     "....."],
    [".....",
     ".0...",
     ".000.",
     ".....",
     "....."],
]

L = [
    [".....",
     "..0..",
     "..0..",
     "..00.",
     "....."],
    [".....",
     "...0.",
     ".000.",
     ".....",
     "....."],
    [".....",
     ".00..",
     "..0..",
     "..0..",
     "....."],
    [".....",
     ".....",
     ".000.",
     ".0...",
     "....."],
]

T = [
    [".....",
     "..0..",
     ".000.",
     ".....",
     "....."],
    [".....",
     "..0..",
     ".00..",
     "..0..",
     "....."],
    [".....",
     ".....",
     ".000.",
     "..0..",
     "....."],
    [".....",
     "..0..",
     "..00.",
     "..0..",
     "....."],
]

shapes = [S, Z, I, O, J, L, T]
shape_colors = [
    (0, 255, 0),
    (255, 0, 0),
    (0, 255, 255),
    (255, 255, 0),
    (255, 165, 0),
    (0, 0, 255),
    (128, 0, 128),
]
# index 0 - 6 will be used to represent the shapes.


class Piece(object):
    rows = 20  # y
    col = 10  # x

    def __init__(self, col, row, shape):
        self.x = col
        self.y = row
        self.shape = shape
        self.color = shape_colors[shapes.index(shape)]
        self.rotation = 0


def create_grid(locked_positions={}):
    """The grid is a 2-dimensional list full of colors. These colors can be seen as parts of the Tetris pieces.
    We create one list of 10 colors, for each of the 20 rows. This then represents our grid.
    There are also 'locked positions', which are pieces that have been placed. The color of these is fixed. These
    have a structure like so: {(col, row): (R, G, B)}."""

    block = (0, 0, 0)
    grid = [[block for _ in range(10)] for _ in range(20)]

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            if (j, i) in locked_positions:
                c = locked_positions[(j, i)]
                grid[i][j] = c

    return grid


def convert_shape_format(shape):
    """Our shape formats in its current form (nested lists) are unreadable. We want to convert this, so the game knows
    what the pieces actually look like.
    This works as follows: it loops through the lists and when it finds a '0', it adds this position to another list.
    The problem is that we have some trailing and following zeroes on the x-axis and empty rows on the y-axis (as in
    the current format from line 28-128). We offset this by subtracting 2 from every x and 4 from every y. This thus
    moves everything to the left and up, so it looks normal."""

    positions = []
    form = shape.shape[shape.rotation % len(shape.shape)]

    for i, line in enumerate(form):
        row = list(line)
        for j, col in enumerate(row):
            if col == "0":
                positions.append((shape.x + j, shape.y + i))

    for i, pos in enumerate(positions):
        positions[i] = (pos[0] - 2, pos[1] - 4)

    return positions


def valid_space(shape, grid):
    """We check the grid to see if the block is moving into a valid space. First you get a list of all the possible
    places in the grid, or all the 'empty' spaces, which are white in our case (we put this into a flattened list
    because this is easy to loop through). We then fit the shapes into the grid and see if it overlaps with anything.
    If it doesn't. It is valid. There is one exception: when the shape 'spawns', it is off-screen, and starts falling.
    This is allowed. The pos[1] is -1 here, so we allow this, but not lower."""

    white_block = (0, 0, 0)

    accepted_pos = [
        [(j, i) for j in range(10) if grid[i][j] == white_block] for i in range(20)
    ]
    accepted_pos = [j for sub in accepted_pos for j in sub]

    formatted = convert_shape_format(shape)

    for pos in formatted:
        if pos not in accepted_pos:
            if pos[1] > -1:
                return False
    return True


def check_lost(positions):
    """Checks if any of the positions are 'lost', or above the screen."""

    for pos in positions:
        x, y = pos
        if y < 1:
            return True

    return False


def get_shape():
    """Returns a random shape of the shape list."""
    return Piece(5, 0, random.choice(shapes))


def update_hold_piece(hold_shape, current_shape):
    """Gives opportunity to hold a piece. If there is no piece in the holding slot, it stashes it, if there is, it
    replaces the piece."""

    if hold_shape is None:
        new_hold_shape = current_shape
        new_current_shape = get_shape()
        return new_hold_shape, new_current_shape

    else:
        new_hold_shape = current_shape
        new_current_shape = hold_shape
        new_current_shape.x, new_current_shape.y = current_shape.x, current_shape.y
        return new_hold_shape, new_current_shape


def draw_text_middle(text, size, color, surface):
    """Draws something to the middle of the screen."""

    font = pygame.font.SysFont("comicsans", size, bold=True)
    label = font.render(text, 1, color)

    surface.blit(
        label,
        (
            top_left_x + play_width / 2 - (label.get_width() / 2),
            top_left_y + play_height / 2 - label.get_height() / 2,
        ),
    )


def draw_grid(surface, grid):
    """Draws the lines on top of the surface, so we can actually see all the blocks."""

    sx = top_left_x
    sy = top_left_y

    for i in range(len(grid)):
        pygame.draw.line(
            surface,
            (128, 128, 128),
            (sx, sy + i * block_size),
            (sx + play_width, sy + i * block_size),
        )
        for j in range(len(grid[i])):
            pygame.draw.line(
                surface,
                (128, 128, 128),
                (sx + j * block_size, sy),
                (sx + j * block_size, sy + play_height),
            )


def clear_rows(grid, locked):
    """Loops to through the rows backwards. If there is not an empty block in the row, it is cleared and everything
    else moves down and another row is added (because we do not shift rows, but delete them).
    This function also returns the number of rows cleared, which we can use towards the score."""

    empty_block = (0, 0, 0)

    inc = 0
    for i in range(len(grid) - 1, -1, -1):
        row = grid[i]
        if empty_block not in row:
            inc += 1
            ind = i
            for j in range(len(row)):
                try:
                    del locked[(j, i)]
                except:
                    continue

    if inc > 0:
        # Why we sort this is because the positions are not in the correct order. We want to remove all values of a
        # certain row, but we cannot simply remove some indices, because these don't line up. We need all the positions
        # with the same y-value.
        for key in sorted(list(locked), key=lambda x: x[1])[::-1]:
            x, y = key
            # Only shift down if the y-value of the key is above the row that needs to be deleted. The inc is there to
            # display how many rows need to shift down
            if y < ind:
                newKey = (x, y + inc)
                locked[newKey] = locked.pop(key)

    return inc


def draw_next_shape(shape, surface):
    """This displays the upcoming shape."""

    font = pygame.font.SysFont("comicsans", 30)
    label = font.render("Next Shape", 1, (255, 255, 255))

    sx = top_left_x + play_width + 50
    sy = top_left_y + play_height / 2 - 100
    form = shape.shape[shape.rotation % len(shape.shape)]

    for i, line in enumerate(form):
        row = list(line)
        for j, col in enumerate(row):
            if col == "0":
                pygame.draw.rect(
                    surface,
                    shape.color,
                    (sx + j * block_size, sy + i * block_size, block_size, block_size),
                    0,
                )

    surface.blit(label, (sx + 10, sy - 30))


def draw_holding_shape(shape, surface):
    """This displays the upcoming shape."""

    font = pygame.font.SysFont("comicsans", 30)
    label = font.render("Holding Shape", 1, (255, 255, 255))

    sx = top_left_x + play_width - 495
    sy = top_left_y + play_height / 2 - 100

    surface.blit(label, (sx + 10, sy - 30))

    if shape is None:
        return

    else:
        form = shape.shape[shape.rotation % len(shape.shape)]
        for i, line in enumerate(form):
            row = list(line)
            for j, col in enumerate(row):
                if col == "0":
                    pygame.draw.rect(
                        surface,
                        shape.color,
                        (sx + j * block_size, sy + i * block_size, block_size, block_size),
                        0,
                    )


def draw_window(surface, grid, score=0):
    """This creates a window on which the blocks and such can be drawn and for each position, a block (or rectangle
    with position col, row and size blocksize, blocksize) is drawn. The total score is also plotted here."""

    surface.fill((0, 0, 0))

    pygame.font.init()
    font = pygame.font.SysFont("comicsans", 60)
    label = font.render("TETRIS", 1, (255, 255, 255))

    surface.blit(label, (top_left_x + play_width / 2 - (label.get_width() / 2), 30))

    # Current Score
    font = pygame.font.SysFont("comicsans", 30)
    label = font.render("Score: " + str(score), 1, (255, 255, 255))

    sx = top_left_x + play_width + 50
    sy = top_left_y + play_height / 2 - 100

    surface.blit(label, (sx + 28, sy + 160))

    # High Score
    high_score = read_max_score()
    label = font.render("High Score: " + high_score, 1, (255, 255, 255))

    sx = top_left_x - 220
    sy = top_left_y + 205

    surface.blit(label, (sx + 28, sy + 160))

    for i in range(len(grid)):
        for j in range(len(grid[i])):
            pygame.draw.rect(
                surface,
                grid[i][j],
                (
                    top_left_x + j * block_size,
                    top_left_y + i * block_size,
                    block_size,
                    block_size,
                ),
                0,
            )

    pygame.draw.rect(
        surface, (255, 0, 0), (top_left_x, top_left_y, play_width, play_height), 4
    )

    draw_grid(surface, grid)


def update_score(new_score):
    """Compares the score from the 'scores.txt' file with the new score and keeps the highest one."""

    score = read_max_score()

    with open("scores.txt", "w") as f:
        if int(score) > new_score:
            f.write(str(score))
        else:
            f.write(str(new_score))


def read_max_score():
    """Reads in the score from the 'scores.txt' file."""

    with open("scores.txt", "r") as f:
        lines = f.readlines()
        score = lines[0].strip()

    return score


def main(win):
    """This function defines some variables and defines what happens when certain keys get pressed (while checking
    for certain limitations)."""

    locked_positions = {}
    grid = create_grid(locked_positions)

    change_piece = False
    run = True
    current_piece = get_shape()
    next_piece = get_shape()
    holding_piece = None
    clock = pygame.time.Clock()
    fall_time = 0
    fall_speed = 0.27
    level_time = 0
    score = 0

    while run:
        # The grid needs to be constantly updated
        grid = create_grid(locked_positions)
        fall_time += clock.get_rawtime()
        level_time += clock.get_rawtime()
        clock.tick()

        if level_time / 1000 > 5:
            level_time = 0
            if fall_speed > 0.12:
                fall_speed -= 0.005

        if fall_time / 1000 > fall_speed:
            fall_time = 0
            current_piece.y += 1
            # We only move vertically, so if this fails, we must have hit the bottom of the screen or another piece, so
            # we need to stop moving this piece and lock it and generate the next piece
            if not (valid_space(current_piece, grid)) and current_piece.y > 0:
                current_piece.y -= 1
                change_piece = True

        for event in pygame.event.get():
            # This gives us a way out of the game
            if event.type == pygame.QUIT:
                run = False
                pygame.display.quit()

            # This provides further inputs to the game. There are also failsafes which result in the block not moving
            # if it is not allowed to do so.
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    current_piece.x -= 1
                    if not (valid_space(current_piece, grid)):
                        current_piece.x += 1
                if event.key == pygame.K_RIGHT:
                    current_piece.x += 1
                    if not (valid_space(current_piece, grid)):
                        current_piece.x -= 1
                if event.key == pygame.K_UP:
                    current_piece.rotation += 1
                    if not (valid_space(current_piece, grid)):
                        current_piece.rotation -= 1
                if event.key == pygame.K_SPACE:
                    holding_piece, current_piece = update_hold_piece(
                        holding_piece, current_piece
                    )

        # This is looked at separately, because holding a button is handy, but only for the down button. The timing
        # for turning around is otherwise hard. I could fiddle around with this more, but this works.
        key_state = pygame.key.get_pressed()
        if key_state[pygame.K_DOWN]:
            pygame.time.delay(25)
            current_piece.y += 1
            if not (valid_space(current_piece, grid)):
                current_piece.y -= 1

        shape_pos = convert_shape_format(current_piece)

        # This gives the colors to the grid, so we can see the blocks
        for i in range(len(shape_pos)):
            x, y = shape_pos[i]
            if y > -1:
                grid[y][x] = current_piece.color

        # Locked positions has this format {(col, row): (R, G, B)}, so we need to add these colors to the dict
        if change_piece:
            for pos in shape_pos:
                p = (pos[0], pos[1])
                locked_positions[p] = current_piece.color
            current_piece = next_piece
            next_piece = get_shape()
            change_piece = False
            # This is suboptimal. The scoring should be different, and when this is changed, clear_rows can be called
            # normally.
            score += clear_rows(grid, locked_positions) * 10

        draw_window(win, grid, score)
        draw_next_shape(next_piece, win)
        draw_holding_shape(holding_piece, win)
        pygame.display.update()

        if check_lost(locked_positions):
            # Display losing message
            win.fill((0, 0, 0))
            draw_text_middle("You Lost!", 80, (255, 255, 255), win)
            pygame.display.update()
            pygame.time.delay(2000)

            # Display high score
            update_score(score)
            high_score = read_max_score()
            win.fill((0, 0, 0))
            draw_text_middle(
                "High Score: {}".format(high_score), 80, (255, 255, 255), win
            )
            pygame.display.update()
            pygame.time.delay(2000)
            run = False


def main_menu(win):
    """This creates a main menu before the game that when a button is pressed, leads to the actual game, which is a
    game that loops until the player loses, which prompts the main menu again."""

    run = True
    while run:
        win.fill((0, 0, 0))
        draw_text_middle("Press Any Key To Play", 60, (255, 255, 255), win)
        pygame.display.update()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN:
                main(win)

    pygame.display.quit()


win = pygame.display.set_mode((s_width, s_height))
pygame.display.set_caption("Tetris")
main_menu(win)
